import 'package:flutter_test/flutter_test.dart';
import 'dart:math' as math;

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    print('最大的数${math.max(2, 4)}');
    print('最小的数${math.min(2, 4)}');

    print('x的exponent次方${math.pow(3, 2)}');
    print('x的正平方根${math.sqrt(9)}');

    print('e的x次方${math.exp(3)}'); //e的x次方 e=2.718281828459045
    print('x的正平方根${math.log(20.085536923187668)}'); //x e的平方根
  });
}
