import 'package:flutter_test/flutter_test.dart';
import 'package:vector_math/vector_math_64.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // 创建一个3D向量
    Vector3 v1 = Vector3(1.0, 2.0, 3.0);
    Vector3 v2 = Vector3(4.0, 5.0, 6.0);

    // 向量加法
    Vector3 resultAdd = v1 + v2;

    // 向量减法
    Vector3 resultSub = v1 - v2;

    // 向量乘法（点积）
    double resultDot = v1.dot(v2);

    // 向量乘法（叉积）
    Vector3 resultCross = v1.cross(v2);

    // 向量的长度
    double length = v1.length;

    // 向量的范数（长度的平方）
    double norm = v1.length2;

    // 单位向量
    Vector3 unit = v1.normalized();

    // 输出结果
    print('向量加法结果: $resultAdd');
    print('向量减法结果: $resultSub');
    print('向量点积结果: $resultDot');
    print('向量叉积结果: $resultCross');
    print('向量长度: $length');
    print('向量范数: $norm');
    print('单位向量: $unit');
  });
}
