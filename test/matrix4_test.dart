import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    Matrix4 matrix = Matrix4.identity();
    print('$matrix');

    Matrix4 translateMatrix4 = Matrix4.identity()..translate(2.0, 3.0, 4.0);
    print('\n$translateMatrix4');

    Matrix4 scaleMatrix4 = Matrix4.identity()..scale(2.0, 3.0, 4.0);
    print('\n$scaleMatrix4');
  });
}
