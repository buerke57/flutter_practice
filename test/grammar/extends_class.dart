import 'dart:developer';

import 'abstract_class.dart';

/// 1.extends（继承），在flutter中继承是单继承。
///
// 子类重写超类的方法要用@override
// 子类调用超类的方法要用super
// 子类会继承父类里面可见的属性和方法，但是不会继承构造函数
//子类能复写父类的getter 和 setter 方法
// 子类可以继承父类的非私有变量

abstract class Walk {
  void walk();
}

abstract class Person implements Walk {
  String? name;
  int? age;

  Person(this.name, this.age);

  //思想是私有的，使用_thought 对子类不可见
  String? _thought;

  //计算这个人类是否成年
  bool get isAdult => (age ?? 0) >= 18;

  void run() {
    log("运行 person 类了");
  }
}

class Student extends Person {
  // Student(super.name, super.age);
  Student(String name, {int? age}) : super(name, age);

  @override
  void walk() {}
}

abstract class Run {
  int speed = 0;

  void run() {
    log("run");
  }
}

abstract class Eat {
  void eat();
}

class Person1 implements Run, Eat {
  @override
  int speed = 10;

  @override
  void eat() {
    log("Person 吃了 $speed 个萝卜");
  }

  @override
  void run() {
    log("Person 跑了 $speed 公里");
  }
}

mixin Speak {
  void speak() {
    log(" 说话了");
  }
}

class Tiger extends Run with Speak {
  @override
  void eat() {
    // TODO: implement eat
  }

  @override
  void speak() {
    super.speak();
    print("Tiger 说话了");
  }
}
