import 'dart:developer';

/// abstract 抽象类
///
//dart抽象类主要用于定义标准，子类可以继承抽象类，也可以实现抽象类接口。
//抽象类通过abstract 关键字来定义。
//抽象类不可以被实例化，只有继承他的子类可以。
//子类继承抽象类必须实现里面的抽象方法。
//抽象类作为接口，必须实现抽象类里面的所有属性和方法。
//多态就是父类定义一个属性或者方法，父类不去实现，让子类去实现，这就叫多态。

//跑
abstract class Run {
  void run();

  void walk() {
    print("我不跑 只走路");
  }
}

//吃
abstract class Eat {
  void eat();
}

//吃
abstract class Call {
  void call();
}

//目的 集成一个抽象方法
abstract class Animal extends Run implements Eat {
  printInfo() {
    print("我是动物 打印一下自己的信息");
  }
}

class Dog extends Animal {
  @override
  void eat() {
    print("我是狗在吃骨头");
  }

  @override
  void run() {
    print("我是狗 在跑");
  }
}

class Cat implements Animal {
  @override
  void eat() {
    print("我是猫在吃鱼");
  }

  @override
  void run() {
    print("我是猫 在跑");
  }

  @override
  printInfo() {
    print("我是猫 打印一下自己的信息");
  }

  @override
  void walk() {
    print("我是猫 慢慢地走");
  }
}

mixin TextMixin {
  void a() {
    print("aaaaa--->>>");
  }

  void b();
}

// class MixinModel with Animal {
//   @override
//   void b() {
//
//   }
// }
