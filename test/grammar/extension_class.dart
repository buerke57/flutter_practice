/// extension（扩展）
///
extension TextOverflowUtil on String {
  String toCharacterBreakStr() {
    if (isEmpty) {
      return this;
    }
    var breakWorkds = '';
    for (var element in runes) {
      breakWorkds += String.fromCharCode(element);
      breakWorkds += '\u200B';
    }
    return breakWorkds;
  }
}
