///城市列表
List cityData = [
  {
    "cityId": "2413",
    "cityName": "阿坝藏族羌族自治州",
    "code": "513200",
    "hotCity": false,
    "pinyin": "a ba zang zu qiang zu zi zhi zhou"
  },
  {
    "cityId": "3136",
    "cityName": "阿克苏地区",
    "code": "652900",
    "hotCity": false,
    "pinyin": "a ke su di qu"
  },
  {
    "cityId": "456",
    "cityName": "阿拉善盟",
    "code": "152900",
    "hotCity": false,
    "pinyin": "a la shan meng"
  },
  {
    "cityId": "3193",
    "cityName": "阿勒泰地区",
    "code": "654300",
    "hotCity": false,
    "pinyin": "a le tai di qu"
  },
  {
    "cityId": "2782",
    "cityName": "阿里地区",
    "code": "542500",
    "hotCity": false,
    "pinyin": "a li di qu"
  },
  {
    "cityId": "2889",
    "cityName": "安康市",
    "code": "610900",
    "hotCity": false,
    "pinyin": "an kang shi"
  },
  {
    "cityId": "1062",
    "cityName": "安庆市",
    "code": "340800",
    "hotCity": false,
    "pinyin": "an qing shi"
  },
  {
    "cityId": "486",
    "cityName": "鞍山市",
    "code": "210300",
    "hotCity": false,
    "pinyin": "an shan shi"
  },
  {
    "cityId": "2496",
    "cityName": "安顺市",
    "code": "520400",
    "hotCity": false,
    "pinyin": "an shun shi"
  },
  {
    "cityId": "1543",
    "cityName": "安阳市",
    "code": "410500",
    "hotCity": false,
    "pinyin": "an yang shi"
  },
  {
    "cityId": "416",
    "cityName": "巴彦淖尔市",
    "code": "150800",
    "hotCity": false,
    "pinyin": "ba yan nao er shi"
  },
  {
    "cityId": "3126",
    "cityName": "巴音郭楞蒙古自治州",
    "code": "652800",
    "hotCity": false,
    "pinyin": "ba yin guo leng meng gu zi zhi zhou"
  },
  {
    "cityId": "2403",
    "cityName": "巴中市",
    "code": "511900",
    "hotCity": false,
    "pinyin": "ba zhong shi"
  },
  {
    "cityId": "630",
    "cityName": "白城市",
    "code": "220800",
    "hotCity": false,
    "pinyin": "bai cheng shi"
  },
  {
    "cityId": "2146",
    "cityName": "百色市",
    "code": "451000",
    "hotCity": false,
    "pinyin": "bai se shi"
  },
  {
    "cityId": "617",
    "cityName": "白山市",
    "code": "220600",
    "hotCity": false,
    "pinyin": "bai shan shi"
  },
  {
    "cityId": "2922",
    "cityName": "白银市",
    "code": "620400",
    "hotCity": false,
    "pinyin": "bai yin shi"
  },
  {
    "cityId": "1029",
    "cityName": "蚌埠市",
    "code": "340300",
    "hotCity": false,
    "pinyin": "bang bu shi"
  },
  {
    "cityId": "121",
    "cityName": "保定市",
    "code": "130600",
    "hotCity": false,
    "pinyin": "bao ding shi"
  },
  {
    "cityId": "2810",
    "cityName": "宝鸡市",
    "code": "610300",
    "hotCity": false,
    "pinyin": "bao ji shi"
  },
  {
    "cityId": "2598",
    "cityName": "保山市",
    "code": "530500",
    "hotCity": false,
    "pinyin": "bao shan shi"
  },
  {
    "cityId": "355",
    "cityName": "包头市",
    "code": "150200",
    "hotCity": false,
    "pinyin": "bao tou shi"
  },
  {
    "cityId": "2117",
    "cityName": "北海市",
    "code": "450500",
    "hotCity": false,
    "pinyin": "bei hai shi"
  },
  {
    "cityId": "3216",
    "cityName": "北京市",
    "code": "110010",
    "hotCity": false,
    "pinyin": "bei jing shi"
  },
  {
    "cityId": "502",
    "cityName": "本溪市",
    "code": "210500",
    "hotCity": false,
    "pinyin": "ben xi shi"
  },
  {
    "cityId": "2503",
    "cityName": "毕节市",
    "code": "520500",
    "hotCity": false,
    "pinyin": "bi jie shi"
  },
  {
    "cityId": "1474",
    "cityName": "滨州市",
    "code": "371600",
    "hotCity": false,
    "pinyin": "bin zhou shi"
  },
  {
    "cityId": "3121",
    "cityName": "博尔塔拉蒙古自治州",
    "code": "652700",
    "hotCity": false,
    "pinyin": "bo er ta la meng gu zi zhi zhou"
  },
  {
    "cityId": "1113",
    "cityName": "亳州市",
    "code": "341600",
    "hotCity": false,
    "pinyin": "bo zhou shi"
  },
  {
    "cityId": "175",
    "cityName": "沧州市",
    "code": "130900",
    "hotCity": false,
    "pinyin": "cang zhou shi"
  },
  {
    "cityId": "1848",
    "cityName": "常德市",
    "code": "430700",
    "hotCity": false,
    "pinyin": "chang de shi"
  },
  {
    "cityId": "2737",
    "cityName": "昌都市",
    "code": "540300",
    "hotCity": false,
    "pinyin": "chang dou shi"
  },
  {
    "cityId": "3113",
    "cityName": "昌吉回族自治州",
    "code": "652300",
    "hotCity": false,
    "pinyin": "chang ji hui zu zi zhi zhou"
  },
  {
    "cityId": "829",
    "cityName": "常州市",
    "code": "320400",
    "hotCity": false,
    "pinyin": "chang zhou shi"
  },
  {
    "cityId": "560",
    "cityName": "朝阳市",
    "code": "211300",
    "hotCity": false,
    "pinyin": "chao yang shi"
  },
  {
    "cityId": "2050",
    "cityName": "潮州市",
    "code": "445100",
    "hotCity": false,
    "pinyin": "chao zhou shi"
  },
  {
    "cityId": "1870",
    "cityName": "郴州市",
    "code": "431000",
    "hotCity": false,
    "pinyin": "chen zhou shi"
  },
  {
    "cityId": "163",
    "cityName": "承德市",
    "code": "130800",
    "hotCity": false,
    "pinyin": "cheng de shi"
  },
  {
    "cityId": "2260",
    "cityName": "成都市",
    "code": "510100",
    "hotCity": false,
    "pinyin": "cheng dou shi"
  },
  {
    "cityId": "369",
    "cityName": "赤峰市",
    "code": "150400",
    "hotCity": false,
    "pinyin": "chi feng shi"
  },
  {
    "cityId": "1118",
    "cityName": "池州市",
    "code": "341700",
    "hotCity": false,
    "pinyin": "chi zhou shi"
  },
  {
    "cityId": "2184",
    "cityName": "崇左市",
    "code": "451400",
    "hotCity": false,
    "pinyin": "chong zuo shi"
  },
  {
    "cityId": "2642",
    "cityName": "楚雄彝族自治州",
    "code": "532300",
    "hotCity": false,
    "pinyin": "chu xiong yi zu zi zhi zhou"
  },
  {
    "cityId": "1081",
    "cityName": "滁州市",
    "code": "341100",
    "hotCity": false,
    "pinyin": "chu zhou shi"
  },
  {
    "cityId": "2680",
    "cityName": "大理白族自治州",
    "code": "532900",
    "hotCity": false,
    "pinyin": "da li bai zu zi zhi zhou"
  },
  {
    "cityId": "475",
    "cityName": "大连市",
    "code": "210200",
    "hotCity": false,
    "pinyin": "da lian shi"
  },
  {
    "cityId": "710",
    "cityName": "大庆市",
    "code": "230600",
    "hotCity": false,
    "pinyin": "da qing shi"
  },
  {
    "cityId": "227",
    "cityName": "大同市",
    "code": "140200",
    "hotCity": false,
    "pinyin": "da tong shi"
  },
  {
    "cityId": "776",
    "cityName": "大兴安岭地区",
    "code": "232700",
    "hotCity": false,
    "pinyin": "da xing an ling di qu"
  },
  {
    "cityId": "2386",
    "cityName": "达州市",
    "code": "511700",
    "hotCity": false,
    "pinyin": "da zhou shi"
  },
  {
    "cityId": "509",
    "cityName": "丹东市",
    "code": "210600",
    "hotCity": false,
    "pinyin": "dan dong shi"
  },
  {
    "cityId": "2204",
    "cityName": "儋州市",
    "code": "460400",
    "hotCity": false,
    "pinyin": "dan zhou shi"
  },
  {
    "cityId": "2693",
    "cityName": "德宏傣族景颇族自治州",
    "code": "533100",
    "hotCity": false,
    "pinyin": "de hong dai zu jing po zu zi zhi zhou"
  },
  {
    "cityId": "2302",
    "cityName": "德阳市",
    "code": "510600",
    "hotCity": false,
    "pinyin": "de yang shi"
  },
  {
    "cityId": "1453",
    "cityName": "德州市",
    "code": "371400",
    "hotCity": false,
    "pinyin": "de zhou shi"
  },
  {
    "cityId": "2704",
    "cityName": "迪庆藏族自治州",
    "code": "533400",
    "hotCity": false,
    "pinyin": "di qing zang zu zi zhi zhou"
  },
  {
    "cityId": "2973",
    "cityName": "定西市",
    "code": "621100",
    "hotCity": false,
    "pinyin": "ding xi shi"
  },
  {
    "cityId": "2048",
    "cityName": "东莞市",
    "code": "441900",
    "hotCity": false,
    "pinyin": "dong guan shi"
  },
  {
    "cityId": "1379",
    "cityName": "东营市",
    "code": "370500",
    "hotCity": false,
    "pinyin": "dong ying shi"
  },
  {
    "cityId": "391",
    "cityName": "鄂尔多斯市",
    "code": "150600",
    "hotCity": false,
    "pinyin": "e er duo si shi"
  },
  {
    "cityId": "1723",
    "cityName": "鄂州市",
    "code": "420700",
    "hotCity": false,
    "pinyin": "e zhou shi"
  },
  {
    "cityId": "1772",
    "cityName": "恩施土家族苗族自治州",
    "code": "422800",
    "hotCity": false,
    "pinyin": "en shi tu jia zu miao zu zi zhi zhou"
  },
  {
    "cityId": "2122",
    "cityName": "防城港市",
    "code": "450600",
    "hotCity": false,
    "pinyin": "fang cheng gang shi"
  },
  {
    "cityId": "1968",
    "cityName": "佛山市",
    "code": "440600",
    "hotCity": false,
    "pinyin": "fo shan shi"
  },
  {
    "cityId": "494",
    "cityName": "抚顺市",
    "code": "210400",
    "hotCity": false,
    "pinyin": "fu shun shi"
  },
  {
    "cityId": "531",
    "cityName": "阜新市",
    "code": "210900",
    "hotCity": false,
    "pinyin": "fu xin shi"
  },
  {
    "cityId": "1090",
    "cityName": "阜阳市",
    "code": "341200",
    "hotCity": false,
    "pinyin": "fu yang shi"
  },
  {
    "cityId": "1132",
    "cityName": "福州市",
    "code": "350100",
    "hotCity": false,
    "pinyin": "fu zhou shi"
  },
  {
    "cityId": "1313",
    "cityName": "抚州市",
    "code": "361000",
    "hotCity": false,
    "pinyin": "fu zhou shi"
  },
  {
    "cityId": "3000",
    "cityName": "甘南藏族自治州",
    "code": "623000",
    "hotCity": false,
    "pinyin": "gan nan zang zu zi zhi zhou"
  },
  {
    "cityId": "1269",
    "cityName": "赣州市",
    "code": "360700",
    "hotCity": false,
    "pinyin": "gan zhou shi"
  },
  {
    "cityId": "2427",
    "cityName": "甘孜藏族自治州",
    "code": "513300",
    "hotCity": false,
    "pinyin": "gan zi zang zu zi zhi zhou"
  },
  {
    "cityId": "3080",
    "cityName": "固原市",
    "code": "640400",
    "hotCity": false,
    "pinyin": "gu yuan shi"
  },
  {
    "cityId": "2379",
    "cityName": "广安市",
    "code": "511600",
    "hotCity": false,
    "pinyin": "guang an shi"
  },
  {
    "cityId": "2319",
    "cityName": "广元市",
    "code": "510800",
    "hotCity": false,
    "pinyin": "guang yuan shi"
  },
  {
    "cityId": "1923",
    "cityName": "广州市",
    "code": "440100",
    "hotCity": false,
    "pinyin": "guang zhou shi"
  },
  {
    "cityId": "2132",
    "cityName": "贵港市",
    "code": "450800",
    "hotCity": false,
    "pinyin": "gui gang shi"
  },
  {
    "cityId": "2091",
    "cityName": "桂林市",
    "code": "450300",
    "hotCity": false,
    "pinyin": "gui lin shi"
  },
  {
    "cityId": "2465",
    "cityName": "贵阳市",
    "code": "520100",
    "hotCity": false,
    "pinyin": "gui yang shi"
  },
  {
    "cityId": "3041",
    "cityName": "果洛藏族自治州",
    "code": "632600",
    "hotCity": false,
    "pinyin": "guo luo zang zu zi zhi zhou"
  },
  {
    "cityId": "646",
    "cityName": "哈尔滨市",
    "code": "230100",
    "hotCity": false,
    "pinyin": "ha er bin shi"
  },
  {
    "cityId": "3109",
    "cityName": "哈密市",
    "code": "650500",
    "hotCity": false,
    "pinyin": "ha mi shi"
  },
  {
    "cityId": "3025",
    "cityName": "海北藏族自治州",
    "code": "632200",
    "hotCity": false,
    "pinyin": "hai bei zang zu zi zhi zhou"
  },
  {
    "cityId": "3018",
    "cityName": "海东市",
    "code": "630200",
    "hotCity": false,
    "pinyin": "hai dong shi"
  },
  {
    "cityId": "2193",
    "cityName": "海口市",
    "code": "460100",
    "hotCity": false,
    "pinyin": "hai kou shi"
  },
  {
    "cityId": "3035",
    "cityName": "海南藏族自治州",
    "code": "632500",
    "hotCity": false,
    "pinyin": "hai nan zang zu zi zhi zhou"
  },
  {
    "cityId": "3055",
    "cityName": "海西蒙古族藏族自治州",
    "code": "632800",
    "hotCity": false,
    "pinyin": "hai xi meng gu zu zang zu zi zhi zhou"
  },
  {
    "cityId": "82",
    "cityName": "邯郸市",
    "code": "130400",
    "hotCity": false,
    "pinyin": "han dan shi"
  },
  {
    "cityId": "2864",
    "cityName": "汉中市",
    "code": "610700",
    "hotCity": false,
    "pinyin": "han zhong shi"
  },
  {
    "cityId": "908",
    "cityName": "杭州市",
    "code": "330100",
    "hotCity": false,
    "pinyin": "hang zhou shi"
  },
  {
    "cityId": "1553",
    "cityName": "鹤壁市",
    "code": "410600",
    "hotCity": false,
    "pinyin": "he bi shi"
  },
  {
    "cityId": "2165",
    "cityName": "河池市",
    "code": "451200",
    "hotCity": false,
    "pinyin": "he chi shi"
  },
  {
    "cityId": "1010",
    "cityName": "合肥市",
    "code": "340100",
    "hotCity": false,
    "pinyin": "he fei shi"
  },
  {
    "cityId": "692",
    "cityName": "鹤岗市",
    "code": "230400",
    "hotCity": false,
    "pinyin": "he gang shi"
  },
  {
    "cityId": "3164",
    "cityName": "和田地区",
    "code": "653200",
    "hotCity": false,
    "pinyin": "he tian di qu"
  },
  {
    "cityId": "2027",
    "cityName": "河源市",
    "code": "441600",
    "hotCity": false,
    "pinyin": "he yuan shi"
  },
  {
    "cityId": "1482",
    "cityName": "菏泽市",
    "code": "371700",
    "hotCity": false,
    "pinyin": "he ze shi"
  },
  {
    "cityId": "2159",
    "cityName": "贺州市",
    "code": "451100",
    "hotCity": false,
    "pinyin": "he zhou shi"
  },
  {
    "cityId": "758",
    "cityName": "黑河市",
    "code": "231100",
    "hotCity": false,
    "pinyin": "hei he shi"
  },
  {
    "cityId": "203",
    "cityName": "衡水市",
    "code": "131100",
    "hotCity": false,
    "pinyin": "heng shui shi"
  },
  {
    "cityId": "1812",
    "cityName": "衡阳市",
    "code": "430400",
    "hotCity": false,
    "pinyin": "heng yang shi"
  },
  {
    "cityId": "2653",
    "cityName": "红河哈尼族彝族自治州",
    "code": "532500",
    "hotCity": false,
    "pinyin": "hong he ha ni zu yi zu zi zhi zhou"
  },
  {
    "cityId": "345",
    "cityName": "呼和浩特市",
    "code": "150100",
    "hotCity": false,
    "pinyin": "hu he hao te shi"
  },
  {
    "cityId": "568",
    "cityName": "葫芦岛市",
    "code": "211400",
    "hotCity": false,
    "pinyin": "hu lu dao shi"
  },
  {
    "cityId": "401",
    "cityName": "呼伦贝尔市",
    "code": "150700",
    "hotCity": false,
    "pinyin": "hu lun bei er shi"
  },
  {
    "cityId": "954",
    "cityName": "湖州市",
    "code": "330500",
    "hotCity": false,
    "pinyin": "hu zhou shi"
  },
  {
    "cityId": "862",
    "cityName": "淮安市",
    "code": "320800",
    "hotCity": false,
    "pinyin": "huai an shi"
  },
  {
    "cityId": "1052",
    "cityName": "淮北市",
    "code": "340600",
    "hotCity": false,
    "pinyin": "huai bei shi"
  },
  {
    "cityId": "1894",
    "cityName": "怀化市",
    "code": "431200",
    "hotCity": false,
    "pinyin": "huai hua shi"
  },
  {
    "cityId": "1037",
    "cityName": "淮南市",
    "code": "340400",
    "hotCity": false,
    "pinyin": "huai nan shi"
  },
  {
    "cityId": "1750",
    "cityName": "黄冈市",
    "code": "421100",
    "hotCity": false,
    "pinyin": "huang gang shi"
  },
  {
    "cityId": "3030",
    "cityName": "黄南藏族自治州",
    "code": "632300",
    "hotCity": false,
    "pinyin": "huang nan zang zu zi zhi zhou"
  },
  {
    "cityId": "1073",
    "cityName": "黄山市",
    "code": "341000",
    "hotCity": false,
    "pinyin": "huang shan shi"
  },
  {
    "cityId": "1683",
    "cityName": "黄石市",
    "code": "420200",
    "hotCity": false,
    "pinyin": "huang shi shi"
  },
  {
    "cityId": "2007",
    "cityName": "惠州市",
    "code": "441300",
    "hotCity": false,
    "pinyin": "hui zhou shi"
  },
  {
    "cityId": "1288",
    "cityName": "吉安市",
    "code": "360800",
    "hotCity": false,
    "pinyin": "ji an shi"
  },
  {
    "cityId": "587",
    "cityName": "吉林市",
    "code": "220200",
    "hotCity": false,
    "pinyin": "ji lin shi"
  },
  {
    "cityId": "1339",
    "cityName": "济南市",
    "code": "370100",
    "hotCity": false,
    "pinyin": "ji nan shi"
  },
  {
    "cityId": "1411",
    "cityName": "济宁市",
    "code": "370800",
    "hotCity": false,
    "pinyin": "ji ning shi"
  },
  {
    "cityId": "682",
    "cityName": "鸡西市",
    "code": "230300",
    "hotCity": false,
    "pinyin": "ji xi shi"
  },
  {
    "cityId": "731",
    "cityName": "佳木斯市",
    "code": "230800",
    "hotCity": false,
    "pinyin": "jia mu si shi"
  },
  {
    "cityId": "946",
    "cityName": "嘉兴市",
    "code": "330400",
    "hotCity": false,
    "pinyin": "jia xing shi"
  },
  {
    "cityId": "2918",
    "cityName": "嘉峪关市",
    "code": "620200",
    "hotCity": false,
    "pinyin": "jia yu guan shi"
  },
  {
    "cityId": "1974",
    "cityName": "江门市",
    "code": "440700",
    "hotCity": false,
    "pinyin": "jiang men shi"
  },
  {
    "cityId": "1572",
    "cityName": "焦作市",
    "code": "410800",
    "hotCity": false,
    "pinyin": "jiao zuo shi"
  },
  {
    "cityId": "2054",
    "cityName": "揭阳市",
    "code": "445200",
    "hotCity": false,
    "pinyin": "jie yang shi"
  },
  {
    "cityId": "2919",
    "cityName": "金昌市",
    "code": "620300",
    "hotCity": false,
    "pinyin": "jin chang shi"
  },
  {
    "cityId": "257",
    "cityName": "晋城市",
    "code": "140500",
    "hotCity": false,
    "pinyin": "jin cheng shi"
  },
  {
    "cityId": "967",
    "cityName": "金华市",
    "code": "330700",
    "hotCity": false,
    "pinyin": "jin hua shi"
  },
  {
    "cityId": "271",
    "cityName": "晋中市",
    "code": "140700",
    "hotCity": false,
    "pinyin": "jin zhong shi"
  },
  {
    "cityId": "516",
    "cityName": "锦州市",
    "code": "210700",
    "hotCity": false,
    "pinyin": "jin zhou shi"
  },
  {
    "cityId": "1237",
    "cityName": "景德镇市",
    "code": "360200",
    "hotCity": false,
    "pinyin": "jing de zhen shi"
  },
  {
    "cityId": "1727",
    "cityName": "荆门市",
    "code": "420800",
    "hotCity": false,
    "pinyin": "jing men shi"
  },
  {
    "cityId": "1741",
    "cityName": "荆州市",
    "code": "421000",
    "hotCity": false,
    "pinyin": "jing zhou shi"
  },
  {
    "cityId": "1248",
    "cityName": "九江市",
    "code": "360400",
    "hotCity": false,
    "pinyin": "jiu jiang shi"
  },
  {
    "cityId": "2956",
    "cityName": "酒泉市",
    "code": "620900",
    "hotCity": false,
    "pinyin": "jiu quan shi"
  },
  {
    "cityId": "3151",
    "cityName": "喀什地区",
    "code": "653100",
    "hotCity": false,
    "pinyin": "ka shen di qu"
  },
  {
    "cityId": "1506",
    "cityName": "开封市",
    "code": "410200",
    "hotCity": false,
    "pinyin": "kai feng shi"
  },
  {
    "cityId": "3100",
    "cityName": "克拉玛依市",
    "code": "650200",
    "hotCity": false,
    "pinyin": "ke la ma yi shi"
  },
  {
    "cityId": "3146",
    "cityName": "克孜勒苏柯尔克孜自治州",
    "code": "653000",
    "hotCity": false,
    "pinyin": "ke zi le su ke er ke zi zi zhi zhou"
  },
  {
    "cityId": "2563",
    "cityName": "昆明市",
    "code": "530100",
    "hotCity": false,
    "pinyin": "kun ming shi"
  },
  {
    "cityId": "2709",
    "cityName": "拉萨市",
    "code": "540100",
    "hotCity": false,
    "pinyin": "la sa shi"
  },
  {
    "cityId": "2177",
    "cityName": "来宾市",
    "code": "451300",
    "hotCity": false,
    "pinyin": "lai bin shi"
  },
  {
    "cityId": "2909",
    "cityName": "兰州市",
    "code": "620100",
    "hotCity": false,
    "pinyin": "lan zhou shi"
  },
  {
    "cityId": "192",
    "cityName": "廊坊市",
    "code": "131000",
    "hotCity": false,
    "pinyin": "lang fang shi"
  },
  {
    "cityId": "2339",
    "cityName": "乐山市",
    "code": "511100",
    "hotCity": false,
    "pinyin": "le shan shi"
  },
  {
    "cityId": "2616",
    "cityName": "丽江市",
    "code": "530700",
    "hotCity": false,
    "pinyin": "li jiang shi"
  },
  {
    "cityId": "999",
    "cityName": "丽水市",
    "code": "331100",
    "hotCity": false,
    "pinyin": "li shui shi"
  },
  {
    "cityId": "855",
    "cityName": "连云港市",
    "code": "320700",
    "hotCity": false,
    "pinyin": "lian yun gang shi"
  },
  {
    "cityId": "2446",
    "cityName": "凉山彝族自治州",
    "code": "513400",
    "hotCity": false,
    "pinyin": "liang shan yi zu zi zhi zhou"
  },
  {
    "cityId": "1465",
    "cityName": "聊城市",
    "code": "371500",
    "hotCity": false,
    "pinyin": "liao cheng shi"
  },
  {
    "cityId": "539",
    "cityName": "辽阳市",
    "code": "211000",
    "hotCity": false,
    "pinyin": "liao yang shi"
  },
  {
    "cityId": "604",
    "cityName": "辽源市",
    "code": "220400",
    "hotCity": false,
    "pinyin": "liao yuan shi"
  },
  {
    "cityId": "2633",
    "cityName": "临沧市",
    "code": "530900",
    "hotCity": false,
    "pinyin": "lin cang shi"
  },
  {
    "cityId": "312",
    "cityName": "临汾市",
    "code": "141000",
    "hotCity": false,
    "pinyin": "lin fen shi"
  },
  {
    "cityId": "2991",
    "cityName": "临夏回族自治州",
    "code": "622900",
    "hotCity": false,
    "pinyin": "lin xia hui zu zi zhi zhou"
  },
  {
    "cityId": "1440",
    "cityName": "临沂市",
    "code": "371300",
    "hotCity": false,
    "pinyin": "lin yi shi"
  },
  {
    "cityId": "2749",
    "cityName": "林芝市",
    "code": "540400",
    "hotCity": false,
    "pinyin": "lin zhi shi"
  },
  {
    "cityId": "1105",
    "cityName": "六安市",
    "code": "341500",
    "hotCity": false,
    "pinyin": "liu an shi"
  },
  {
    "cityId": "2476",
    "cityName": "六盘水市",
    "code": "520200",
    "hotCity": false,
    "pinyin": "liu pan shui shi"
  },
  {
    "cityId": "2080",
    "cityName": "柳州市",
    "code": "450200",
    "hotCity": false,
    "pinyin": "liu zhou shi"
  },
  {
    "cityId": "2981",
    "cityName": "陇南市",
    "code": "621200",
    "hotCity": false,
    "pinyin": "long nan shi"
  },
  {
    "cityId": "1208",
    "cityName": "龙岩市",
    "code": "350800",
    "hotCity": false,
    "pinyin": "long yan shi"
  },
  {
    "cityId": "1907",
    "cityName": "娄底市",
    "code": "431300",
    "hotCity": false,
    "pinyin": "lou di shi"
  },
  {
    "cityId": "2294",
    "cityName": "泸州市",
    "code": "510500",
    "hotCity": false,
    "pinyin": "lu zhou shi"
  },
  {
    "cityId": "1597",
    "cityName": "漯河市",
    "code": "411100",
    "hotCity": false,
    "pinyin": "luo he shi"
  },
  {
    "cityId": "1516",
    "cityName": "洛阳市",
    "code": "410300",
    "hotCity": false,
    "pinyin": "luo yang shi"
  },
  {
    "cityId": "330",
    "cityName": "吕梁市",
    "code": "141100",
    "hotCity": false,
    "pinyin": "lv liang shi"
  },
  {
    "cityId": "1045",
    "cityName": "马鞍山市",
    "code": "340500",
    "hotCity": false,
    "pinyin": "ma an shan shi"
  },
  {
    "cityId": "1992",
    "cityName": "茂名市",
    "code": "440900",
    "hotCity": false,
    "pinyin": "mao ming shi"
  },
  {
    "cityId": "2361",
    "cityName": "眉山市",
    "code": "511400",
    "hotCity": false,
    "pinyin": "mei shan shi"
  },
  {
    "cityId": "2013",
    "cityName": "梅州市",
    "code": "441400",
    "hotCity": false,
    "pinyin": "mei zhou shi"
  },
  {
    "cityId": "2309",
    "cityName": "绵阳市",
    "code": "510700",
    "hotCity": false,
    "pinyin": "mian yang shi"
  },
  {
    "cityId": "747",
    "cityName": "牡丹江市",
    "code": "231000",
    "hotCity": false,
    "pinyin": "mu dan jiang shi"
  },
  {
    "cityId": "1227",
    "cityName": "南昌市",
    "code": "360100",
    "hotCity": false,
    "pinyin": "nan chang shi"
  },
  {
    "cityId": "2351",
    "cityName": "南充市",
    "code": "511300",
    "hotCity": false,
    "pinyin": "nan chong shi"
  },
  {
    "cityId": "798",
    "cityName": "南京市",
    "code": "320100",
    "hotCity": false,
    "pinyin": "nan jing shi"
  },
  {
    "cityId": "2067",
    "cityName": "南宁市",
    "code": "450100",
    "hotCity": false,
    "pinyin": "nan ning shi"
  },
  {
    "cityId": "1197",
    "cityName": "南平市",
    "code": "350700",
    "hotCity": false,
    "pinyin": "nan ping shi"
  },
  {
    "cityId": "846",
    "cityName": "南通市",
    "code": "320600",
    "hotCity": false,
    "pinyin": "nan tong shi"
  },
  {
    "cityId": "1610",
    "cityName": "南阳市",
    "code": "411300",
    "hotCity": false,
    "pinyin": "nan yang shi"
  },
  {
    "cityId": "2333",
    "cityName": "内江市",
    "code": "511000",
    "hotCity": false,
    "pinyin": "nei jiang shi"
  },
  {
    "cityId": "2770",
    "cityName": "那曲市",
    "code": "540600",
    "hotCity": false,
    "pinyin": "nei qu shi"
  },
  {
    "cityId": "922",
    "cityName": "宁波市",
    "code": "330200",
    "hotCity": false,
    "pinyin": "ning bo shi"
  },
  {
    "cityId": "1216",
    "cityName": "宁德市",
    "code": "350900",
    "hotCity": false,
    "pinyin": "ning de shi"
  },
  {
    "cityId": "2699",
    "cityName": "怒江傈僳族自治州",
    "code": "533300",
    "hotCity": false,
    "pinyin": "nu jiang li su zu zi zhi zhou"
  },
  {
    "cityId": "547",
    "cityName": "盘锦市",
    "code": "211100",
    "hotCity": false,
    "pinyin": "pan jin shi"
  },
  {
    "cityId": "2288",
    "cityName": "攀枝花市",
    "code": "510400",
    "hotCity": false,
    "pinyin": "pan zhi hua shi"
  },
  {
    "cityId": "1532",
    "cityName": "平顶山市",
    "code": "410400",
    "hotCity": false,
    "pinyin": "ping ding shan shi"
  },
  {
    "cityId": "2948",
    "cityName": "平凉市",
    "code": "620800",
    "hotCity": false,
    "pinyin": "ping liang shi"
  },
  {
    "cityId": "1242",
    "cityName": "萍乡市",
    "code": "360300",
    "hotCity": false,
    "pinyin": "ping xiang shi"
  },
  {
    "cityId": "2622",
    "cityName": "普洱市",
    "code": "530800",
    "hotCity": false,
    "pinyin": "pu er shi"
  },
  {
    "cityId": "1153",
    "cityName": "莆田市",
    "code": "350300",
    "hotCity": false,
    "pinyin": "pu tian shi"
  },
  {
    "cityId": "1583",
    "cityName": "濮阳市",
    "code": "410900",
    "hotCity": false,
    "pinyin": "pu yang shi"
  },
  {
    "cityId": "665",
    "cityName": "齐齐哈尔市",
    "code": "230200",
    "hotCity": false,
    "pinyin": "qi qi ha er shi"
  },
  {
    "cityId": "742",
    "cityName": "七台河市",
    "code": "230900",
    "hotCity": false,
    "pinyin": "qi tai he shi"
  },
  {
    "cityId": "2532",
    "cityName": "黔东南苗族侗族自治州",
    "code": "522600",
    "hotCity": false,
    "pinyin": "qian dong nan miao zu dong zu zi zhi zhou"
  },
  {
    "cityId": "2549",
    "cityName": "黔南布依族苗族自治州",
    "code": "522700",
    "hotCity": false,
    "pinyin": "qian nan bu yi zu miao zu zi zhi zhou"
  },
  {
    "cityId": "2523",
    "cityName": "黔西南布依族苗族自治州",
    "code": "522300",
    "hotCity": false,
    "pinyin": "qian xi nan bu yi zu miao zu zi zhi zhou"
  },
  {
    "cityId": "74",
    "cityName": "秦皇岛市",
    "code": "130300",
    "hotCity": false,
    "pinyin": "qin huang dao shi"
  },
  {
    "cityId": "2127",
    "cityName": "钦州市",
    "code": "450700",
    "hotCity": false,
    "pinyin": "qin zhou shi"
  },
  {
    "cityId": "1352",
    "cityName": "青岛市",
    "code": "370200",
    "hotCity": false,
    "pinyin": "qing dao shi"
  },
  {
    "cityId": "2964",
    "cityName": "庆阳市",
    "code": "621000",
    "hotCity": false,
    "pinyin": "qing yang shi"
  },
  {
    "cityId": "2039",
    "cityName": "清远市",
    "code": "441800",
    "hotCity": false,
    "pinyin": "qing yuan shi"
  },
  {
    "cityId": "2578",
    "cityName": "曲靖市",
    "code": "530300",
    "hotCity": false,
    "pinyin": "qu jing shi"
  },
  {
    "cityId": "977",
    "cityName": "衢州市",
    "code": "330800",
    "hotCity": false,
    "pinyin": "qu zhou shi"
  },
  {
    "cityId": "1172",
    "cityName": "泉州市",
    "code": "350500",
    "hotCity": false,
    "pinyin": "quan zhou shi"
  },
  {
    "cityId": "2718",
    "cityName": "日喀则市",
    "code": "540200",
    "hotCity": false,
    "pinyin": "ri ka ze shi"
  },
  {
    "cityId": "1435",
    "cityName": "日照市",
    "code": "371100",
    "hotCity": false,
    "pinyin": "ri zhao shi"
  },
  {
    "cityId": "1603",
    "cityName": "三门峡市",
    "code": "411200",
    "hotCity": false,
    "pinyin": "san men xia shi"
  },
  {
    "cityId": "1159",
    "cityName": "三明市",
    "code": "350400",
    "hotCity": false,
    "pinyin": "san ming shi"
  },
  {
    "cityId": "2203",
    "cityName": "三沙市",
    "code": "460300",
    "hotCity": false,
    "pinyin": "san sha shi"
  },
  {
    "cityId": "2198",
    "cityName": "三亚市",
    "code": "460200",
    "hotCity": false,
    "pinyin": "san ya shi"
  },
  {
    "cityId": "1146",
    "cityName": "厦门市",
    "code": "350200",
    "hotCity": false,
    "pinyin": "sha men shi"
  },
  {
    "cityId": "2757",
    "cityName": "山南市",
    "code": "540500",
    "hotCity": false,
    "pinyin": "shan nan shi"
  },
  {
    "cityId": "1960",
    "cityName": "汕头市",
    "code": "440500",
    "hotCity": false,
    "pinyin": "shan tou shi"
  },
  {
    "cityId": "2022",
    "cityName": "汕尾市",
    "code": "441500",
    "hotCity": false,
    "pinyin": "shan wei shi"
  },
  {
    "cityId": "3218",
    "cityName": "上海市",
    "code": "310010",
    "hotCity": false,
    "pinyin": "shang hai shi"
  },
  {
    "cityId": "2900",
    "cityName": "商洛市",
    "code": "611000",
    "hotCity": false,
    "pinyin": "shang luo shi"
  },
  {
    "cityId": "1624",
    "cityName": "商丘市",
    "code": "411400",
    "hotCity": false,
    "pinyin": "shang qiu shi"
  },
  {
    "cityId": "1325",
    "cityName": "上饶市",
    "code": "361100",
    "hotCity": false,
    "pinyin": "shang rao shi"
  },
  {
    "cityId": "1935",
    "cityName": "韶关市",
    "code": "440200",
    "hotCity": false,
    "pinyin": "shao guan shi"
  },
  {
    "cityId": "960",
    "cityName": "绍兴市",
    "code": "330600",
    "hotCity": false,
    "pinyin": "shao xing shi"
  },
  {
    "cityId": "1825",
    "cityName": "邵阳市",
    "code": "430500",
    "hotCity": false,
    "pinyin": "shao yang shi"
  },
  {
    "cityId": "461",
    "cityName": "沈阳市",
    "code": "210100",
    "hotCity": false,
    "pinyin": "shen yang shi"
  },
  {
    "cityId": "1946",
    "cityName": "深圳市",
    "code": "440300",
    "hotCity": false,
    "pinyin": "shen zhen shi"
  },
  {
    "cityId": "36",
    "cityName": "石家庄市",
    "code": "130100",
    "hotCity": false,
    "pinyin": "shi jia zhuang shi"
  },
  {
    "cityId": "1690",
    "cityName": "十堰市",
    "code": "420300",
    "hotCity": false,
    "pinyin": "shi yan shi"
  },
  {
    "cityId": "3070",
    "cityName": "石嘴山市",
    "code": "640200",
    "hotCity": false,
    "pinyin": "shi zui shan shi"
  },
  {
    "cityId": "701",
    "cityName": "双鸭山市",
    "code": "230500",
    "hotCity": false,
    "pinyin": "shuang ya shan shi"
  },
  {
    "cityId": "264",
    "cityName": "朔州市",
    "code": "140600",
    "hotCity": false,
    "pinyin": "shuo zhou shi"
  },
  {
    "cityId": "597",
    "cityName": "四平市",
    "code": "220300",
    "hotCity": false,
    "pinyin": "si ping shi"
  },
  {
    "cityId": "624",
    "cityName": "松原市",
    "code": "220700",
    "hotCity": false,
    "pinyin": "song yuan shi"
  },
  {
    "cityId": "901",
    "cityName": "宿迁市",
    "code": "321300",
    "hotCity": false,
    "pinyin": "su qian shi"
  },
  {
    "cityId": "836",
    "cityName": "苏州市",
    "code": "320500",
    "hotCity": false,
    "pinyin": "su zhou shi"
  },
  {
    "cityId": "1099",
    "cityName": "宿州市",
    "code": "341300",
    "hotCity": false,
    "pinyin": "su zhou shi"
  },
  {
    "cityId": "765",
    "cityName": "绥化市",
    "code": "231200",
    "hotCity": false,
    "pinyin": "sui hua shi"
  },
  {
    "cityId": "2327",
    "cityName": "遂宁市",
    "code": "510900",
    "hotCity": false,
    "pinyin": "sui ning shi"
  },
  {
    "cityId": "1768",
    "cityName": "随州市",
    "code": "421300",
    "hotCity": false,
    "pinyin": "sui zhou shi"
  },
  {
    "cityId": "3185",
    "cityName": "塔城地区",
    "code": "654200",
    "hotCity": false,
    "pinyin": "ta cheng di qu"
  },
  {
    "cityId": "1423",
    "cityName": "泰安市",
    "code": "370900",
    "hotCity": false,
    "pinyin": "tai an shi"
  },
  {
    "cityId": "216",
    "cityName": "太原市",
    "code": "140100",
    "hotCity": false,
    "pinyin": "tai yuan shi"
  },
  {
    "cityId": "894",
    "cityName": "泰州市",
    "code": "321200",
    "hotCity": false,
    "pinyin": "tai zhou shi"
  },
  {
    "cityId": "989",
    "cityName": "台州市",
    "code": "331000",
    "hotCity": false,
    "pinyin": "tai zhou shi"
  },
  {
    "cityId": "59",
    "cityName": "唐山市",
    "code": "130200",
    "hotCity": false,
    "pinyin": "tang shan shi"
  },
  {
    "cityId": "3217",
    "cityName": "天津市",
    "code": "120010",
    "hotCity": false,
    "pinyin": "tian jin shi"
  },
  {
    "cityId": "2928",
    "cityName": "天水市",
    "code": "620500",
    "hotCity": false,
    "pinyin": "tian shui shi"
  },
  {
    "cityId": "552",
    "cityName": "铁岭市",
    "code": "211200",
    "hotCity": false,
    "pinyin": "tie ling shi"
  },
  {
    "cityId": "2805",
    "cityName": "铜川市",
    "code": "610200",
    "hotCity": false,
    "pinyin": "tong chuan shi"
  },
  {
    "cityId": "609",
    "cityName": "通化市",
    "code": "220500",
    "hotCity": false,
    "pinyin": "tong hua shi"
  },
  {
    "cityId": "382",
    "cityName": "通辽市",
    "code": "150500",
    "hotCity": false,
    "pinyin": "tong liao shi"
  },
  {
    "cityId": "1057",
    "cityName": "铜陵市",
    "code": "340700",
    "hotCity": false,
    "pinyin": "tong ling shi"
  },
  {
    "cityId": "2512",
    "cityName": "铜仁市",
    "code": "520600",
    "hotCity": false,
    "pinyin": "tong ren shi"
  },
  {
    "cityId": "3105",
    "cityName": "吐鲁番市",
    "code": "650400",
    "hotCity": false,
    "pinyin": "tu lu fan shi"
  },
  {
    "cityId": "1398",
    "cityName": "潍坊市",
    "code": "370700",
    "hotCity": false,
    "pinyin": "wei fang shi"
  },
  {
    "cityId": "1430",
    "cityName": "威海市",
    "code": "371000",
    "hotCity": false,
    "pinyin": "wei hai shi"
  },
  {
    "cityId": "2838",
    "cityName": "渭南市",
    "code": "610500",
    "hotCity": false,
    "pinyin": "wei nan shi"
  },
  {
    "cityId": "2667",
    "cityName": "文山壮族苗族自治州",
    "code": "532600",
    "hotCity": false,
    "pinyin": "wen shan zhuang zu miao zu zi zhi zhou"
  },
  {
    "cityId": "933",
    "cityName": "温州市",
    "code": "330300",
    "hotCity": false,
    "pinyin": "wen zhou shi"
  },
  {
    "cityId": "365",
    "cityName": "乌海市",
    "code": "150300",
    "hotCity": false,
    "pinyin": "wu hai shi"
  },
  {
    "cityId": "1669",
    "cityName": "武汉市",
    "code": "420100",
    "hotCity": false,
    "pinyin": "wu han shi"
  },
  {
    "cityId": "1020",
    "cityName": "芜湖市",
    "code": "340200",
    "hotCity": false,
    "pinyin": "wu hu shi"
  },
  {
    "cityId": "424",
    "cityName": "乌兰察布市",
    "code": "150900",
    "hotCity": false,
    "pinyin": "wu lan cha bu shi"
  },
  {
    "cityId": "3091",
    "cityName": "乌鲁木齐市",
    "code": "650100",
    "hotCity": false,
    "pinyin": "wu lu mu qi shi"
  },
  {
    "cityId": "2936",
    "cityName": "武威市",
    "code": "620600",
    "hotCity": false,
    "pinyin": "wu wei shi"
  },
  {
    "cityId": "810",
    "cityName": "无锡市",
    "code": "320200",
    "hotCity": false,
    "pinyin": "wu xi shi"
  },
  {
    "cityId": "3074",
    "cityName": "吴忠市",
    "code": "640300",
    "hotCity": false,
    "pinyin": "wu zhong shi"
  },
  {
    "cityId": "2109",
    "cityName": "梧州市",
    "code": "450400",
    "hotCity": false,
    "pinyin": "wu zhou shi"
  },
  {
    "cityId": "2791",
    "cityName": "西安市",
    "code": "610100",
    "hotCity": false,
    "pinyin": "xi an shi"
  },
  {
    "cityId": "443",
    "cityName": "锡林郭勒盟",
    "code": "152500",
    "hotCity": false,
    "pinyin": "xi lin guo le meng"
  },
  {
    "cityId": "3010",
    "cityName": "西宁市",
    "code": "630100",
    "hotCity": false,
    "pinyin": "xi ning shi"
  },
  {
    "cityId": "2676",
    "cityName": "西双版纳傣族自治州",
    "code": "532800",
    "hotCity": false,
    "pinyin": "xi shuang ban na dai zu zi zhi zhou"
  },
  {
    "cityId": "1761",
    "cityName": "咸宁市",
    "code": "421200",
    "hotCity": false,
    "pinyin": "xian ning shi"
  },
  {
    "cityId": "2823",
    "cityName": "咸阳市",
    "code": "610400",
    "hotCity": false,
    "pinyin": "xian yang shi"
  },
  {
    "cityId": "1806",
    "cityName": "湘潭市",
    "code": "430300",
    "hotCity": false,
    "pinyin": "xiang tan shi"
  },
  {
    "cityId": "1913",
    "cityName": "湘西土家族苗族自治州",
    "code": "433100",
    "hotCity": false,
    "pinyin": "xiang xi tu jia zu miao zu zi zhi zhou"
  },
  {
    "cityId": "1713",
    "cityName": "襄阳市",
    "code": "420600",
    "hotCity": false,
    "pinyin": "xiang yang shi"
  },
  {
    "cityId": "1733",
    "cityName": "孝感市",
    "code": "420900",
    "hotCity": false,
    "pinyin": "xiao gan shi"
  },
  {
    "cityId": "1559",
    "cityName": "新乡市",
    "code": "410700",
    "hotCity": false,
    "pinyin": "xin xiang shi"
  },
  {
    "cityId": "1634",
    "cityName": "信阳市",
    "code": "411500",
    "hotCity": false,
    "pinyin": "xin yang shi"
  },
  {
    "cityId": "1262",
    "cityName": "新余市",
    "code": "360500",
    "hotCity": false,
    "pinyin": "xin yu shi"
  },
  {
    "cityId": "297",
    "cityName": "忻州市",
    "code": "140900",
    "hotCity": false,
    "pinyin": "xin zhou shi"
  },
  {
    "cityId": "436",
    "cityName": "兴安盟",
    "code": "152200",
    "hotCity": false,
    "pinyin": "xing an meng"
  },
  {
    "cityId": "101",
    "cityName": "邢台市",
    "code": "130500",
    "hotCity": false,
    "pinyin": "xing tai shi"
  },
  {
    "cityId": "1590",
    "cityName": "许昌市",
    "code": "411000",
    "hotCity": false,
    "pinyin": "xu chang shi"
  },
  {
    "cityId": "818",
    "cityName": "徐州市",
    "code": "320300",
    "hotCity": false,
    "pinyin": "xu zhou shi"
  },
  {
    "cityId": "1123",
    "cityName": "宣城市",
    "code": "341800",
    "hotCity": false,
    "pinyin": "xuan cheng shi"
  },
  {
    "cityId": "2394",
    "cityName": "雅安市",
    "code": "511800",
    "hotCity": false,
    "pinyin": "ya an shi"
  },
  {
    "cityId": "2850",
    "cityName": "延安市",
    "code": "610600",
    "hotCity": false,
    "pinyin": "yan an shi"
  },
  {
    "cityId": "636",
    "cityName": "延边朝鲜族自治州",
    "code": "222400",
    "hotCity": false,
    "pinyin": "yan bian chao xian zu zi zhi zhou"
  },
  {
    "cityId": "870",
    "cityName": "盐城市",
    "code": "320900",
    "hotCity": false,
    "pinyin": "yan cheng shi"
  },
  {
    "cityId": "1385",
    "cityName": "烟台市",
    "code": "370600",
    "hotCity": false,
    "pinyin": "yan tai shi"
  },
  {
    "cityId": "2034",
    "cityName": "阳江市",
    "code": "441700",
    "hotCity": false,
    "pinyin": "yang jiang shi"
  },
  {
    "cityId": "238",
    "cityName": "阳泉市",
    "code": "140300",
    "hotCity": false,
    "pinyin": "yang quan shi"
  },
  {
    "cityId": "880",
    "cityName": "扬州市",
    "code": "321000",
    "hotCity": false,
    "pinyin": "yang zhou shi"
  },
  {
    "cityId": "2368",
    "cityName": "宜宾市",
    "code": "511500",
    "hotCity": false,
    "pinyin": "yi bin shi"
  },
  {
    "cityId": "1699",
    "cityName": "宜昌市",
    "code": "420500",
    "hotCity": false,
    "pinyin": "yi chang shi"
  },
  {
    "cityId": "720",
    "cityName": "伊春市",
    "code": "230700",
    "hotCity": false,
    "pinyin": "yi chun shi"
  },
  {
    "cityId": "1302",
    "cityName": "宜春市",
    "code": "360900",
    "hotCity": false,
    "pinyin": "yi chun shi"
  },
  {
    "cityId": "3173",
    "cityName": "伊犁哈萨克自治州",
    "code": "654000",
    "hotCity": false,
    "pinyin": "yi li ha sa ke zi zhi zhou"
  },
  {
    "cityId": "1863",
    "cityName": "益阳市",
    "code": "430900",
    "hotCity": false,
    "pinyin": "yi yang shi"
  },
  {
    "cityId": "3063",
    "cityName": "银川市",
    "code": "640100",
    "hotCity": false,
    "pinyin": "yin chuan shi"
  },
  {
    "cityId": "524",
    "cityName": "营口市",
    "code": "210800",
    "hotCity": false,
    "pinyin": "ying kou shi"
  },
  {
    "cityId": "1265",
    "cityName": "鹰潭市",
    "code": "360600",
    "hotCity": false,
    "pinyin": "ying tan shi"
  },
  {
    "cityId": "1882",
    "cityName": "永州市",
    "code": "431100",
    "hotCity": false,
    "pinyin": "yong zhou shi"
  },
  {
    "cityId": "2138",
    "cityName": "玉林市",
    "code": "450900",
    "hotCity": false,
    "pinyin": "yu lin shi"
  },
  {
    "cityId": "2876",
    "cityName": "榆林市",
    "code": "610800",
    "hotCity": false,
    "pinyin": "yu lin shi"
  },
  {
    "cityId": "3048",
    "cityName": "玉树藏族自治州",
    "code": "632700",
    "hotCity": false,
    "pinyin": "yu shu zang zu zi zhi zhou"
  },
  {
    "cityId": "2588",
    "cityName": "玉溪市",
    "code": "530400",
    "hotCity": false,
    "pinyin": "yu xi shi"
  },
  {
    "cityId": "1838",
    "cityName": "岳阳市",
    "code": "430600",
    "hotCity": false,
    "pinyin": "yue yang shi"
  },
  {
    "cityId": "283",
    "cityName": "运城市",
    "code": "140800",
    "hotCity": false,
    "pinyin": "yun cheng shi"
  },
  {
    "cityId": "2060",
    "cityName": "云浮市",
    "code": "445300",
    "hotCity": false,
    "pinyin": "yun fu shi"
  },
  {
    "cityId": "1372",
    "cityName": "枣庄市",
    "code": "370400",
    "hotCity": false,
    "pinyin": "zao zhuang shi"
  },
  {
    "cityId": "1982",
    "cityName": "湛江市",
    "code": "440800",
    "hotCity": false,
    "pinyin": "zhan jiang shi"
  },
  {
    "cityId": "576",
    "cityName": "长春市",
    "code": "220100",
    "hotCity": false,
    "pinyin": "zhang chun shi"
  },
  {
    "cityId": "1858",
    "cityName": "张家界市",
    "code": "430800",
    "hotCity": false,
    "pinyin": "zhang jia jie shi"
  },
  {
    "cityId": "146",
    "cityName": "张家口市",
    "code": "130700",
    "hotCity": false,
    "pinyin": "zhang jia kou shi"
  },
  {
    "cityId": "1786",
    "cityName": "长沙市",
    "code": "430100",
    "hotCity": false,
    "pinyin": "zhang sha shi"
  },
  {
    "cityId": "2941",
    "cityName": "张掖市",
    "code": "620700",
    "hotCity": false,
    "pinyin": "zhang ye shi"
  },
  {
    "cityId": "244",
    "cityName": "长治市",
    "code": "140400",
    "hotCity": false,
    "pinyin": "zhang zhi shi"
  },
  {
    "cityId": "1185",
    "cityName": "漳州市",
    "code": "350600",
    "hotCity": false,
    "pinyin": "zhang zhou shi"
  },
  {
    "cityId": "1998",
    "cityName": "肇庆市",
    "code": "441200",
    "hotCity": false,
    "pinyin": "zhao qing shi"
  },
  {
    "cityId": "2604",
    "cityName": "昭通市",
    "code": "530600",
    "hotCity": false,
    "pinyin": "zhao tong shi"
  },
  {
    "cityId": "887",
    "cityName": "镇江市",
    "code": "321100",
    "hotCity": false,
    "pinyin": "zhen jiang shi"
  },
  {
    "cityId": "1493",
    "cityName": "郑州市",
    "code": "410100",
    "hotCity": false,
    "pinyin": "zheng zhou shi"
  },
  {
    "cityId": "3219",
    "cityName": "重庆市",
    "code": "500010",
    "hotCity": false,
    "pinyin": "zhong qing shi"
  },
  {
    "cityId": "2049",
    "cityName": "中山市",
    "code": "442000",
    "hotCity": false,
    "pinyin": "zhong shan shi"
  },
  {
    "cityId": "3086",
    "cityName": "中卫市",
    "code": "640500",
    "hotCity": false,
    "pinyin": "zhong wei shi"
  },
  {
    "cityId": "1645",
    "cityName": "周口市",
    "code": "411600",
    "hotCity": false,
    "pinyin": "zhou kou shi"
  },
  {
    "cityId": "984",
    "cityName": "舟山市",
    "code": "330900",
    "hotCity": false,
    "pinyin": "zhou shan shi"
  },
  {
    "cityId": "1956",
    "cityName": "珠海市",
    "code": "440400",
    "hotCity": false,
    "pinyin": "zhu hai shi"
  },
  {
    "cityId": "1656",
    "cityName": "驻马店市",
    "code": "411700",
    "hotCity": false,
    "pinyin": "zhu ma dian shi"
  },
  {
    "cityId": "1796",
    "cityName": "株洲市",
    "code": "430200",
    "hotCity": false,
    "pinyin": "zhu zhou shi"
  },
  {
    "cityId": "1363",
    "cityName": "淄博市",
    "code": "370300",
    "hotCity": false,
    "pinyin": "zi bo shi"
  },
  {
    "cityId": "2281",
    "cityName": "自贡市",
    "code": "510300",
    "hotCity": false,
    "pinyin": "zi gong shi"
  },
  {
    "cityId": "2409",
    "cityName": "资阳市",
    "code": "512000",
    "hotCity": false,
    "pinyin": "zi yang shi"
  },
  {
    "cityId": "2481",
    "cityName": "遵义市",
    "code": "520300",
    "hotCity": false,
    "pinyin": "zun yi shi"
  }
];

List hotCityData = [
  {
    "cityId": "3216",
    "cityName": "北京市",
    "code": "110010",
    "hotCity": false,
    "pinyin": "bei jing shi"
  },
  {
    "cityId": "2260",
    "cityName": "成都市",
    "code": "510100",
    "hotCity": false,
    "pinyin": "cheng dou shi"
  },
  {
    "cityId": "82",
    "cityName": "邯郸市",
    "code": "130400",
    "hotCity": false,
    "pinyin": "han dan shi"
  },
  {
    "cityId": "3217",
    "cityName": "天津市",
    "code": "120010",
    "hotCity": false,
    "pinyin": "tian jin shi"
  },
  {
    "cityId": "1858",
    "cityName": "张家界市",
    "code": "430800",
    "hotCity": false,
    "pinyin": "zhang jia jie shi"
  },
];
