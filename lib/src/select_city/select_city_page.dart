import 'package:flutter/material.dart';
import 'package:azlistview/azlistview.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import 'select_city_model.dart';
import 'city_bean.dart';

///选择城市
class SelectCityPage extends StatefulWidget {
  const SelectCityPage({super.key});

  @override
  State<SelectCityPage> createState() => _SelectCityPageState();
}

class _SelectCityPageState extends State<SelectCityPage> {
  late SelectCityModel _model;
  final ItemScrollController itemScrollController = ItemScrollController();

  @override
  void initState() {
    _model = SelectCityModel();
    _model.initData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SelectCityPage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: AzListView(
                data: _model.dataList,
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: _model.dataList.length,
                itemBuilder: (BuildContext context, int index) {
                  CityBean model = _model.dataList[index];
                  return getListItem(context, model);
                },
                itemScrollController: itemScrollController,
                susItemBuilder: (BuildContext context, int index) {
                  CityBean model = _model.dataList[index];
                  return getSusItem(context, model.getSuspensionTag());
                },
                indexBarData: ['↑', ...kIndexBarData],
                indexBarOptions: const IndexBarOptions(
                  needRebuild: true,
                  hapticFeedback: true,
                  selectTextStyle: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.w500),
                  selectItemDecoration: BoxDecoration(
                      shape: BoxShape.circle, color: Color(0xFF333333)),
                  indexHintWidth: 96,
                  indexHintHeight: 97,
                  indexHintDecoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/ic_index_bar_bubble_white.png'),
                      fit: BoxFit.contain,
                    ),
                  ),
                  indexHintAlignment: Alignment.centerRight,
                  indexHintTextStyle:
                      TextStyle(fontSize: 24.0, color: Colors.black87),
                  indexHintOffset: Offset(-30, 0),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getSusItem(BuildContext context, String tag, {double susHeight = 40}) {
    if (tag == "↑") return SizedBox();
    return Container(
      height: susHeight,
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(left: 16.0),
      color: const Color(0xFFF3F4F5),
      alignment: Alignment.centerLeft,
      child: Text(
        '$tag',
        softWrap: false,
        style: const TextStyle(
          fontSize: 14.0,
          color: Color(0xFF666666),
        ),
      ),
    );
  }

  Widget getListItem(BuildContext context, CityBean city,
      {double susHeight = 40}) {
    if (city.tagIndex == "↑") {
      return GridView.builder(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            mainAxisSpacing: 8,
            crossAxisSpacing: 8,
            childAspectRatio: 1),
        itemBuilder: (context, index) {
          return Container(color: Colors.red);
        },
        itemCount: 8,
      );
    }
    return ListTile(
      title: Text(city.cityName ?? ''),
      onTap: () {},
    );
  }
}
