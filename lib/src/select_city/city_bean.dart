import 'package:azlistview/azlistview.dart';

//城市
class CityBean extends ISuspensionBean {
  String? cityId;
  String? cityName;
  String? code;
  bool? hotCity;
  String? pinyin;
  String? tagIndex;

  List<String>? children;

  CityBean({
    this.cityId,
    this.cityName,
    this.code,
    this.hotCity,
    this.pinyin,
    this.tagIndex,
    this.children,
  });

  CityBean.fromJson(Map<String, dynamic> json) {
    cityId = json['cityId'];
    cityName = json['cityName'];
    code = json['code'];
    hotCity = json['hotCity'];
    pinyin = json['pinyin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['cityId'] = cityId;
    data['cityName'] = cityName;
    data['code'] = code;
    data['hotCity'] = hotCity;
    data['pinyin'] = pinyin;
    return data;
  }

  @override
  String getSuspensionTag() => tagIndex!;
}
