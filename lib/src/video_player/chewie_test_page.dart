import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';

import 'app_video_player.dart';

///chewie 插件
///
class ChewieTestPage extends StatefulWidget {
  const ChewieTestPage({super.key});

  @override
  State<ChewieTestPage> createState() => _ChewieTestPageState();
}

class _ChewieTestPageState extends State<ChewieTestPage> {
  ChewieController? chewieController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SizedBox(
      width: double.maxFinite,
      child: AppVideoPlayer(
        url:
            "https://assets.mixkit.co/videos/preview/mixkit-spinning-around-the-earth-29351-large.mp4",
        onCreated: (controller) {
          chewieController = controller;
        },
      ),
    );
  }
}
