import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_practice/src/permission/permission_helper.dart';
import 'package:permission_handler/permission_handler.dart';

///权限请求
class PermissionTestPage extends StatefulWidget {
  const PermissionTestPage({super.key});

  @override
  State<PermissionTestPage> createState() => _PermissionTestPageState();
}

class _PermissionTestPageState extends State<PermissionTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PermissionTestPage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SizedBox(
      width: double.maxFinite,
      child: Column(
        children: [
          TextButton(
              onPressed: () async {
                Permission.storage.isDenied; //拒绝状态
                Permission.storage.isGranted; //允许状态
                Permission.storage.isPermanentlyDenied; //限被永久拒绝

                Permission.storage.isProvisional; //仅IOS支持
                Permission.storage.isLimited; //仅IOS支持
                Permission.storage.isRestricted; //仅IOS支持

                PermissionStatus status = await Permission.storage.request();
                log('Permission.storage $status -- >>');
              },
              child: const Text(
                "请求照片和文件",
                style: TextStyle(color: Colors.black, fontSize: 20),
              )),
          TextButton(
              onPressed: () async {
                bool isGranted =
                    await PermissionHelper.checkAndRequestPermission(
                        context, Permission.location);

                log('isGranted --- $isGranted');
              },
              child: const Text(
                "定位",
                style: TextStyle(color: Colors.black, fontSize: 20),
              )),
          TextButton(
              onPressed: () async {},
              child: const Text(
                "点我",
                style: TextStyle(color: Colors.black, fontSize: 20),
              )),
        ],
      ),
    );
  }
}
