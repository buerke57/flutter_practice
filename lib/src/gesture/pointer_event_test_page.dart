import 'dart:ffi';
import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

///
class PointerEventTestPage extends StatefulWidget {
  const PointerEventTestPage({super.key});

  @override
  State<PointerEventTestPage> createState() => _PointerEventTestPageState();
}

class _PointerEventTestPageState extends State<PointerEventTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PointerEventTestPage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    PointerEvent? event;
    Pointer pointer;
    PointerSignalEvent? signalEvent;
    PointerDataPacket packet;
    return const Center(
      child: Text("PointerEventTestPage", style: TextStyle(fontSize: 24)),
    );
  }
}
