import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_practice/main.dart';

///gesture_test_page.dart
class GestureTestPage extends StatefulWidget {
  const GestureTestPage({super.key});

  @override
  State<GestureTestPage> createState() => _GestureTestPageState();
}

class _GestureTestPageState extends State<GestureTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("GestureTestPage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    // final Map<Type, GestureRecognizerFactory> gestures = <Type, GestureRecognizerFactory>{};
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onScaleStart: (details) {
        log('onScaleStart  ---> ${details.toString()}');
      },
      onScaleUpdate: (details) {
        log('onScaleUpdate  ---> ${details.toString()}');
      },
      onScaleEnd: (details) {
        log('onScaleEnd  ---> ${details.toString()}');
      },
      // onTap: () {
      //   log('onTap  --->');
      // },
      // onDoubleTap: () {
      //   log('onDoubleTap  --->');
      // },
      // onLongPress: () {
      //   log('onLongPress  --->');
      // },
      // onVerticalDragStart: (details) {
      //   log('onVerticalDragStart  ---> ${details.toString()}');
      // },
      // onVerticalDragUpdate: (details) {
      //   log('onVerticalDragUpdate  ---> ${details.toString()}');
      // },
      // onVerticalDragEnd: (details) {
      //   log('onVerticalDragEnd  ---> ${details.toString()}');
      // },
      // onVerticalDragCancel: () {
      //   log('onVerticalDragCancel  --->');
      // },
      // onHorizontalDragStart: (details) {
      //   log('onHorizontalDragStart  ---> ${details.toString()}');
      // },
      // onHorizontalDragUpdate: (details) {
      //   log('onHorizontalDragUpdate  ---> ${details.toString()}');
      // },
      // onHorizontalDragEnd: (details) {
      //   log('onHorizontalDragEnd  ---> ${details.toString()}');
      // },
      // onHorizontalDragCancel: () {
      //   log('onHorizontalDragCancel  --->');
      // },
      // onPanStart: (details) {
      //   log('onPanStart  ---> ${details.toString()}');
      // },
      // onPanUpdate: (details) {
      //   log('onPanUpdate  ---> ${details.toString()}');
      // },
      // onPanEnd: (details) {
      //   log('onPanEnd  ---> ${details.toString()}');
      // },
      // onPanCancel: () {
      //   log('onPanCancel  --->');
      // },
      // onForcePressStart: (details) {
      //   log('onForcePressStart  ---> ${details.toString()}');
      // },
      child: Container(
        width: 100,
        height: 100,
        color: Colors.red,
      ),
    );
  }
}
