import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

///
class ListenerTestPage extends StatefulWidget {
  const ListenerTestPage({super.key});

  @override
  State<ListenerTestPage> createState() => _ListenerTestPageState();
}

class _ListenerTestPageState extends State<ListenerTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ListenerTestPage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Listener(
      onPointerDown: (event) {
        log('onPointerDown  ${event.toString()}');
      },
      onPointerMove: (event) {
        log('onPointerMove  ${event.toString()}');
      },
      onPointerUp: (event) {
        log('onPointerUp  ${event.toString()}');
      },
      onPointerCancel: (event) {
        log('onPointerCancel  ${event.toString()}');
      },
      onPointerPanZoomStart: (event) {
        log('onPointerPanZoomStart  ${event.toString()}');
      },
      onPointerPanZoomUpdate: (event) {
        log('onPointerPanZoomUpdate  ${event.toString()}');
      },
      onPointerPanZoomEnd: (event) {
        log('onPointerPanZoomEnd  ${event.toString()}');
      },
      onPointerHover: (event) {
        log('onPointerHover  ${event.toString()}');
      },
      onPointerSignal: (event) {
        log('onPointerSignal  ${event.toString()}');
      },
      child: Container(width: 100, height: 100, color: Colors.red),
    );
  }
}
