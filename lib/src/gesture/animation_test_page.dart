import 'package:flutter/material.dart';

class AnimationTestPage extends StatefulWidget {
  const AnimationTestPage({super.key});

  @override
  State<AnimationTestPage> createState() => _AnimationTestPageState();
}

class _AnimationTestPageState extends State<AnimationTestPage>
    with TickerProviderStateMixin {
  late AnimationController controller;
  double size = 100;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1500),
      lowerBound: 100,
      upperBound: 200,
    )
      ..addStatusListener((status) {
        print('addStatusListener ${status.toString()}');
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      })
      ..addListener(() {
        setState(() {
          size = controller.value;
          print('${size}xxx');
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    // controller.animateBack(target)
    // controller.animateTo(target)
    return Scaffold(
      body: Center(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            controller.forward();
          },
          child: Container(
            width: size,
            height: size,
            color: Colors.red,
          ),
        ),
      ),
    );
  }
}
