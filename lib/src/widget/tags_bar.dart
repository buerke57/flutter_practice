import 'package:flutter/material.dart';

//tag 条
class TagsBar extends StatelessWidget {
  const TagsBar({
    super.key,
    required this.tagList,
    this.color,
    this.style,
    this.height = 18,
    this.space = 6,
  });

  final List<String> tagList;
  final Color? color;
  final TextStyle? style;
  final double height;
  final double space;

  @override
  Widget build(BuildContext context) {
    Color color = this.color ?? Theme.of(context).primaryColor;

    return SizedBox(
      height: height,
      child: ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Container(
              alignment: Alignment.center,
              margin:
                  index == 0 ? EdgeInsets.zero : EdgeInsets.only(left: space),
              padding: const EdgeInsets.symmetric(horizontal: 4),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: color, width: 1),
                  borderRadius: BorderRadius.circular(2)),
              child: Text(tagList[index],
                  textAlign: TextAlign.center,
                  style: style ??
                      TextStyle(
                          fontSize: 8,
                          color: color,
                          height: 1,
                          fontWeight: FontWeight.bold)),
            );
          },
          itemCount: tagList.length),
    );
  }
}
