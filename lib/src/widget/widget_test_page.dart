import 'package:flutter/material.dart';
import 'package:flutter_practice/src/widget/tags_bar.dart';

///widget 测试页面
class WidgetTestPage extends StatefulWidget {
  const WidgetTestPage({super.key});

  @override
  State<WidgetTestPage> createState() => _WidgetTestPageState();
}

class _WidgetTestPageState extends State<WidgetTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("WidgetTestPage")),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Column(
      children: [
        TagsBar(tagList: ["测试", "MAX"]),
      ],
    );
  }
}
