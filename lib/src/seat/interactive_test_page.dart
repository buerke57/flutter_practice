import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' show Matrix4, Quad, Vector3;

class InteractiveTestPage extends StatefulWidget {
  const InteractiveTestPage({super.key});

  @override
  State<InteractiveTestPage> createState() => _InteractiveTestPageState();
}

class _InteractiveTestPageState extends State<InteractiveTestPage> {
  @override
  Widget build(BuildContext context) {
    Matrix4 matrix = Matrix4.identity();
    log('$matrix');
    matrix.translate(1);
    log('$matrix');

    return Scaffold(
      appBar: AppBar(
        title: const Text("手势"),
        centerTitle: true,
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Column(
      children: [
        Expanded(child: _buildSeat()),
      ],
    );
  }

  Widget _buildSeat() {
    Matrix4 matrix = Matrix4.identity();
    matrix.scale(2, 3, 4);
    return InteractiveViewer(
      minScale: 1,
      maxScale: 2.0,
      onInteractionStart: (details) {
        print("onInteractionStart -->> ${details.toString()}");
      },
      onInteractionUpdate: (details) {
        print("onInteractionUpdate -->> ${details.toString()}");
      },
      onInteractionEnd: (details) {
        print("onInteractionEnd -->> ${details.toString()}");
      },
      child: Column(
        children: List.generate(10, (rowIndex) {
          return Row(
            children: List.generate(10, (colIndex) {
              return Container(
                margin: const EdgeInsets.all(2),
                child: GestureDetector(
                  onTap: () {
                    print("点击了 $rowIndex $colIndex");
                  },
                  child: Container(
                    width: 18,
                    height: 18,
                    padding: const EdgeInsets.all(2),
                    child: Image.asset(
                      "assets/seat_unselected.png",
                      width: double.maxFinite,
                      height: double.maxFinite,
                    ),
                  ),
                ),
              );
            }),
          );
        }),
      ),
    );
  }
}
