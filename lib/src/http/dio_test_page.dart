import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

///
class DioTestPage extends StatefulWidget {
  const DioTestPage({super.key});

  @override
  State<DioTestPage> createState() => _DioTestPageState();
}

class _DioTestPageState extends State<DioTestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DioTestPage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return Center(
      child: TextButton(
          onPressed: onClick,
          child: const Text("点击", style: TextStyle(fontSize: 24))),
    );
  }

  String urlPath =
      "https://ftp-nginx.zhihuidev.testx.fzyun.cn/spc/coursePacket/1716362207547.pdf";

  String urlPath1 =
      "https://www.yidureading.com/portal/api/dynamicFile/stream.do?recordID=4141222";

  onClick() async {
    PermissionStatus status = await Permission.storage.request();

    try {
      Dio dio = Dio();
      String savePath = "${await getTemporaryDirectory()}/d/1716362207547.pdf";
      log("$savePath --->>> ");

      // File file = File(savePath);
      // if (await file.exists()) {
      //   file.deleteSync();
      // } else {
      //   await file.create(recursive: true);
      // }

      await dio.download(urlPath1, savePath);
      log("下载完成 --->>> ");
    } catch (e) {
      log("${e.toString()} --->>> ");
    }
  }
}
