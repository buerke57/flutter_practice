import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:wechat_camera_picker/wechat_camera_picker.dart';

///选图
class WechatAssetsPickerCasePage extends StatefulWidget {
  const WechatAssetsPickerCasePage({super.key});

  @override
  State<WechatAssetsPickerCasePage> createState() =>
      _WechatAssetsPickerCasePageState();
}

class _WechatAssetsPickerCasePageState
    extends State<WechatAssetsPickerCasePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("WechatAssetsPickerCasePage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SizedBox.expand(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () async {
              List<AssetEntity>? assets = await AssetPicker.pickAssets(
                context,
                pickerConfig: const AssetPickerConfig(
                  maxAssets: 10,
                  requestType: RequestType.image,
                ),
              );
              log("length=${assets?.length}");
            },
            child: const Text("选择图片", style: TextStyle(fontSize: 24)),
          ),
          TextButton(
            onPressed: () async {
              AssetEntity? asset = await CameraPicker.pickFromCamera(context,
                  pickerConfig: const CameraPickerConfig(
                    enableRecording: false,
                  ));
              log("getMediaUrl=${await asset?.getMediaUrl()}-->>");
            },
            child: const Text("选择图片", style: TextStyle(fontSize: 24)),
          ),
        ],
      ),
    );
  }
}
