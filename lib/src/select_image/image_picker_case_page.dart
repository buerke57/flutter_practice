import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

///选图
class ImagePickerCasePage extends StatefulWidget {
  const ImagePickerCasePage({super.key});

  @override
  State<ImagePickerCasePage> createState() => _ImagePickerCasePageState();
}

class _ImagePickerCasePageState extends State<ImagePickerCasePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ImagePickerCasePage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SizedBox.expand(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () async {
              final XFile? pickedFile =
                  await ImagePicker().pickImage(source: ImageSource.gallery);
              if (pickedFile != null) {
                log("path=${pickedFile.path}");
              } else {
                log("选择图片null");
              }
            },
            child: const Text("选择单个图片", style: TextStyle(fontSize: 24)),
          ),
          TextButton(
            onPressed: () async {
              final List<XFile> pickedFileList =
                  await ImagePicker().pickMultiImage();
              log("length=${pickedFileList.length} path=${pickedFileList.first.path}");
            },
            child: const Text("选择多个图片", style: TextStyle(fontSize: 24)),
          ),
          TextButton(
            onPressed: () async {
              final XFile? pickedFile = await ImagePicker().pickMedia();
              if (pickedFile != null) {
                log("path=${pickedFile.path}");
              } else {
                log("选择媒体null");
              }
            },
            child: const Text("选择单个媒体", style: TextStyle(fontSize: 24)),
          ),
          TextButton(
            onPressed: () async {
              final List<XFile> pickedFileList =
                  await ImagePicker().pickMultipleMedia();
              log("length=${pickedFileList.length} path=${pickedFileList.first.path}");
            },
            child: const Text("选择多个媒体", style: TextStyle(fontSize: 24)),
          ),
        ],
      ),
    );
  }
}
