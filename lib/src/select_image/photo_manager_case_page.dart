import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';

///选图
class PhotoManagerCasePage extends StatefulWidget {
  const PhotoManagerCasePage({super.key});

  @override
  State<PhotoManagerCasePage> createState() => _PhotoManagerCasePageState();
}

class _PhotoManagerCasePageState extends State<PhotoManagerCasePage> {
  final FilterOptionGroup _filterOptionGroup = FilterOptionGroup(
    imageOption: const FilterOption(
      sizeConstraint: SizeConstraint(ignoreSize: true),
    ),
  );

  final int _sizePerPage = 50;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PhotoManagerCasePage"),
      ),
      body: _buildContent(),
    );
  }

  Widget _buildContent() {
    return SizedBox.expand(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () async {
              final PermissionState ps =
                  await PhotoManager.requestPermissionExtend();
              if (!ps.hasAccess) {
                log('Permission is not accessible.');
                return;
              }

              final List<AssetPathEntity> paths =
                  await PhotoManager.getAssetPathList(
                onlyAll: true,
                filterOption: _filterOptionGroup,
              );

              if (paths.isEmpty) {
                log('No paths found.');
                return;
              }
              final List<AssetEntity> entities =
                  await paths.first.getAssetListPaged(
                page: 0,
                size: _sizePerPage,
              );
              log('length=${entities.length}');
            },
            child: const Text("选择图片", style: TextStyle(fontSize: 24)),
          ),
        ],
      ),
    );
  }
}
