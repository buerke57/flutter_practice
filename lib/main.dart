import 'package:flutter/material.dart';

import 'src/gesture/gesture_test_page.dart';
import 'src/gesture/listener_test_page.dart';
import 'src/http/dio_test_page.dart';
import 'src/matrix4/matrix4_page.dart';
import 'src/path/path_provider_page.dart';
import 'src/permission/permission_test_page.dart';
import 'src/photo_view/photo_view_page.dart';
import 'src/photo_view/popup/popup_test_page.dart';
import 'src/seat/interactive_test_page.dart';
import 'src/seat/select_seat_page.dart';
import 'src/seat/select_seat_page.dart';
import 'src/seat/my_seat_page.dart';
import 'src/select_city/select_city_page.dart';
import 'src/select_image/photo_manager_case_page.dart';
import 'src/select_image/wechat_assets_picker_case_page.dart';
import 'src/video_player/chewie_test_page.dart';
import 'src/webview/webview_test_page.dart';
import 'src/widget/widget_test_page.dart';
import 'src/select_image/image_picker_case_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const WebViewTestPage(),
    );
  }
}
